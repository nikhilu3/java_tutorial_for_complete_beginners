
// Interfaces can only abstract methods(methods which don't have body). Their methods should be 
// defined in the sub-class.
// They can only have final fields having syntax as static final.
// Only public fields and methods are allowed in interfaces.
// From java 8 interfaces can have default concrete methods and from java 9 private 
// methods are also allowed in interfaces.
// These are pretty much same as abstract classes but these can't have instance variables and methods.
// Interfaces cannot be instantiated. These provide abstraction property. 

public interface I1 {
    void show(); 
}
