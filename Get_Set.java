
class Person2{
     // Instance variables (data or "state")
	 String name;
	 int age;
	 
	 void speak() {
		 System.out.println("Hello");
	 }
	 
	int calculateYearsToRetirement() {
		int yearsLeft = 65 - age;
		return yearsLeft;
	}
	
	int getAge() {
		return age;
	}
	String getName() {
		return name;
	}
	
}

public class Get_Set {

	public static void main(String[] args) {
		
		Person2 person1 = new Person2();
		person1.name = "Joe Bloggs";
		person1.age = 37;
		person1.speak();
		
		System.out.println(person1.name);
	
		int years = person1.calculateYearsToRetirement();
		
		int age = person1.getAge();
		
		String name = person1.getName();
		
		System.out.println("Name is " + name);
		System.out.println("Age is " +  age);
		System.out.println("Years til retirement " + years);
	}

}
