
class Person{
     // Instance variables (data or "state")
	 String name;
	 int age;
	 
	 void speak() {
		 System.out.println("Hello");
	 }
       
}

public class Subroutines {

	public static void main(String[] args) {
		
		Person person1 = new Person();
		person1.name = "Joe Bloggs";
		person1.age = 37;
		person1.speak();
		
		System.out.println(person1.name);
	}

}
