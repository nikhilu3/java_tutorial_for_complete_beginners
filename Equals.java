public class Equals {
    public static void main(String[] args) {
        String a = "abc";
        String b = "abc";
        String c = new String("abc");
        String d = a;
        String e = new String(d);
        System.out.println(a == b); // true
        System.out.println(a == c); // false
        System.out.println(b == c); // false
        System.out.println(a == d); // true;
        System.out.println(a == e); // false

        // equals compare the actual values
        // == operator checks if the two operands refer to the same address
    }
}
