class Machinex{
	public void start() {
		System.out.println("machine started");
	}
}

class Camera extends Machinex{

	public void start() {
		System.out.println("Camera started");
	}
	
	public void snap() {
		System.out.println("Photo taken");
	}
}



public class Upcasting_Downcasting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Machinex  machine1 = new Machinex();
		Camera camera1 = new Camera();
		
		machine1.start();
		camera1.start();
		camera1.snap();
		
		// Upcasting
		Machinex machine2 = camera1;
		machine2.start();
		// error : machine2.snap(); 
		// variable type decides what functions can be used
		// object referred decides which function to be used
		
		// Downcasting
		Machinex machine3 = new Camera();
		
		Camera camera2 = (Camera) machine3;
		camera2.start();
		camera2.snap();
		
		// Doesn't work .... runtime error
		Machinex machine4 = new Machinex();
		Camera camera3 = (Camera)machine4;
		//camera3.start();
		//camera3.snap();
	}

}
