class Thing{
	
	public final static int LUCKY_NUMBER = 7; 
	
	public String name;
	public static String description;
	public static int count = 0; // To track the no of objects
	
	public int id;
	
	public Thing() {
		id = count;
		count++;
	}
	
	public void showName() {
		System.out.println("object id: " + id + ", " + description);
	}
	
	public static void showInfo() {
		System.out.println(description);
	}
}
public class StaticKey {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Thing.description  = "I am a thing";
		System.out.println(Thing.count);
		Thing thing1 = new Thing();
		Thing thing2 = new Thing();
		
		thing1.name = "Bob";
		thing2.name = "Sue";
		
		thing1.showName();
		thing2.showName();
		
		System.out.println(Thing.count);
		System.out.println(Thing.LUCKY_NUMBER);
	}

}
