class Machine{
	private String name;
	private int code;
	
	public Machine(){
		this("Arnie", 0);

		System.out.println("Constructor running");
	}
	
	public Machine(String name) {
		this(name, 0);
		System.out.println("Second constructor is running");
		this.setName(name);
	}
	
	public Machine(String name, int code) {
		
		
		System.out.println("Third Constructor running");
		this.setName(name);
		this.setCode(code);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}
}


public class Constructors {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Machine machine1 = new Machine();
		
		Machine machine2 = new Machine("Bertie");
		
		Machine machine3 = new Machine("Chalky", 7);
		
	}

}
